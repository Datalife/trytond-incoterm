# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from .incoterm import IncotermMasterMixin


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    customer_incoterms = fields.One2Many('party.customer_incoterm', 'party',
        'Customer Incoterms', states={'readonly': ~Eval('active')},
        depends=['active'])


class PartyCustomerIncoterm(ModelView, ModelSQL, IncotermMasterMixin):
    """Party Customer Incoterm"""
    __name__ = 'party.customer_incoterm'

    party = fields.Many2One('party.party', 'Party', required=True,
        ondelete='CASCADE')

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)

        # Migration from 4.8: rename table
        old_table = 'party_incoterm'
        if table_h.table_exist(old_table):
            table_h.table_rename(old_table, cls._table)

        super().__register__(module_name)
