# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from .incoterm import IncotermMixin, IncotermDocumentMixin


class Invoice(IncotermDocumentMixin, metaclass=PoolMeta):
    __name__ = 'account.invoice'

    incoterms = fields.One2Many('account.invoice.incoterm', 'invoice',
        'Incoterms',
        states={'readonly': Eval('state') != 'draft'},
        depends=['state', 'currency', 'total_amount'],
        context={'currency': Eval('currency'),
                 'value': Eval('total_amount')})

    @fields.depends('party', 'incoterms', 'type')
    def on_change_party(self):
        super(Invoice, self).on_change_party()
        if not self.party or self.type != 'out':
            return
        self._explode_incoterms(self.party.customer_incoterms)


class InvoiceIncoterm(IncotermMixin, ModelSQL, ModelView):
    """Invoice Incoterm"""
    __name__ = 'account.invoice.incoterm'

    invoice = fields.Many2One('account.invoice', 'Invoice', required=True,
        ondelete='CASCADE')

    def _get_relation_version(self):
        return self.invoice

    @fields.depends('invoice', '_parent_invoice.incoterm_version')
    def on_change_with_version(self, name=None):
        return super(InvoiceIncoterm, self).on_change_with_version(name)
