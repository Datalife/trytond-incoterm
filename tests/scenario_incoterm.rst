==================
Incoterm Scenario
==================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()

Install sale::

    >>> config = activate_modules('incoterm')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()

Add incoterms to customer::

    >>> Rule = Model.get('incoterm.rule')
    >>> fob_rule, = Rule.find([('abbreviation', '=', 'FOB')])
    >>> cpt_rule, = Rule.find([('abbreviation', '=', 'CPT')])
    >>> incoterm1 = customer.customer_incoterms.new()
    >>> incoterm1.rule = cpt_rule
    >>> incoterm1.place = 'Alabama'
    >>> incoterm2 = customer.customer_incoterms.new()
    >>> incoterm2.rule = fob_rule
    >>> incoterm2.place = 'Georgia'
    >>> customer.save()
    >>> len(customer.customer_incoterms)
    2

Create category::

    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.cost_price = Decimal('5')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product.template = template
    >>> product.save()

    >>> service = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'service'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.salable = True
    >>> template.list_price = Decimal('30')
    >>> template.cost_price = Decimal('10')
    >>> template.cost_price_method = 'fixed'
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> service.template = template
    >>> service.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Sale 5 products::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.incoterm_version.code
    '2010'
    >>> sale.party = customer
    >>> len(sale.incoterms)
    2
    >>> sorted(i.rule.abbreviation for i in sale.incoterms)
    ['CPT', 'FOB']
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 2.0
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.type = 'comment'
    >>> sale_line.description = 'Comment'
    >>> sale_line = SaleLine()
    >>> sale.lines.append(sale_line)
    >>> sale_line.product = product
    >>> sale_line.quantity = 3.0
    >>> sale.click('quote')
    >>> sale.incoterms[0].value = Decimal(30)
    >>> sale.incoterms[1].value = Decimal(50)
    >>> sale.click('confirm')
    >>> sale.click('process')
    >>> invoice, = sale.invoices
    >>> shipment, = sale.shipments

Check incoterms propagation::

    >>> len(invoice.incoterms)
    2
    >>> invoice.incoterms[0].version.code
    '2010'
    >>> invoice.incoterms[0].rule.abbreviation
    'CPT'
    >>> invoice.incoterms[0].value
    Decimal('30')
    >>> invoice.incoterms[0].place
    'Alabama'
    >>> invoice.incoterms[1].version.code
    '2010'
    >>> invoice.incoterms[1].rule.abbreviation
    'FOB'
    >>> invoice.incoterms[1].value
    Decimal('50')
    >>> invoice.incoterms[1].place
    'Georgia'
    >>> len(shipment.incoterms)
    2
    >>> shipment.incoterms[0].version.code
    '2010'
    >>> shipment.incoterms[0].rule.abbreviation
    'CPT'
    >>> shipment.incoterms[0].value
    Decimal('30')
    >>> shipment.incoterms[0].place
    'Alabama'
    >>> shipment.incoterms[1].version.code
    '2010'
    >>> shipment.incoterms[1].rule.abbreviation
    'FOB'
    >>> shipment.incoterms[1].value
    Decimal('50')
    >>> shipment.incoterms[1].place
    'Georgia'