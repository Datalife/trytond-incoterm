# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from .incoterm import IncotermMixin, IncotermDocumentMixin


class ShipmentOut(IncotermDocumentMixin, metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    incoterms = fields.One2Many('stock.shipment.out.incoterm', 'shipment_out',
        'Incoterms',
        states={'readonly': Eval('state') != 'draft'},
        depends=['state'])

    @fields.depends('customer', 'incoterms')
    def on_change_customer(self):
        super(ShipmentOut, self).on_change_customer()
        if not self.customer:
            return
        self._explode_incoterms(self.customer.customer_incoterms)


class ShipmentOutIncoterm(IncotermMixin, ModelSQL, ModelView):
    """Shipment Out Incoterm"""
    __name__ = 'stock.shipment.out.incoterm'

    shipment_out = fields.Many2One('stock.shipment.out', 'Shipment Out',
        required=True, ondelete='CASCADE')

    def _get_relation_version(self):
        return self.shipment_out

    @fields.depends('shipment_out', '_parent_shipment_out.incoterm_version')
    def on_change_with_version(self, name=None):
        return super(ShipmentOutIncoterm, self).on_change_with_version(name)
