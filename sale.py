# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from .incoterm import IncotermMixin, IncotermDocumentMixin


class Sale(IncotermDocumentMixin, metaclass=PoolMeta):
    __name__ = 'sale.sale'

    incoterms = fields.One2Many('sale.incoterm', 'sale', 'Incoterms',
        states={'readonly': Eval('state') != 'draft'},
        context={
            'currency': Eval('currency'),
            'value': Eval('total_amount')
        },
        depends=['state', 'currency', 'total_amount'])

    def create_invoice(self):
        InvoiceIncoterm = Pool().get('account.invoice.incoterm')

        invoice = super().create_invoice()

        if invoice and self.incoterms:
            invoice.incoterm_version = self.incoterm_version
            incoterms = list(getattr(invoice, 'incoterms', []))
            # consider grouping method
            for incoterm in self.incoterms:
                if incoterm.rule.id in [i.rule.id for i in incoterms]:
                    continue
                incoterms.append(InvoiceIncoterm(rule=incoterm.rule,
                    value=incoterm.value,
                    currency=incoterm.currency,
                    place=incoterm.place,
                    place_required=incoterm.place_required)
                )
            invoice.incoterms = incoterms
            invoice.save()
        return invoice

    def create_shipment(self, shipment_type):
        ShipmentIncoterm = Pool().get('stock.shipment.out.incoterm')

        shipments = super(Sale, self).create_shipment(shipment_type)

        if not shipments:
            return shipments
        if shipment_type != 'out' or not self.incoterms:
            return shipments

        for shipment in shipments:
            shipment.incoterm_version = self.incoterm_version
            shipment.incoterms = [
                ShipmentIncoterm(
                    rule=incoterm.rule,
                    value=incoterm.value,
                    currency=incoterm.currency,
                    place=incoterm.place,
                    place_required=incoterm.place_required)
                for incoterm in self.incoterms]
            shipment.save()
        return shipments

    @fields.depends('party', 'incoterms')
    def on_change_party(self):
        super(Sale, self).on_change_party()
        if not self.party:
            return
        self._explode_incoterms(self.party.customer_incoterms)


class SaleIncoterm(IncotermMixin, ModelSQL, ModelView):
    """Sale Incoterm"""
    __name__ = 'sale.incoterm'

    sale = fields.Many2One('sale.sale', 'Sale', required=True,
        ondelete='CASCADE')

    def _get_relation_version(self):
        return self.sale

    @fields.depends('sale', '_parent_sale.incoterm_version')
    def on_change_with_version(self, name=None):
        return super(SaleIncoterm, self).on_change_with_version(name)
