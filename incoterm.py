# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import Unique
from trytond.pool import Pool
from trytond.model import fields, ModelView, ModelSQL, Model, DeactivableMixin
from trytond.pyson import Eval, Bool
from trytond.transaction import Transaction


class Version(ModelView, ModelSQL, DeactivableMixin):
    """Incoterm version"""
    __name__ = 'incoterm.version'
    _rec_name = 'code'

    code = fields.Char('Code', required=True)

    @classmethod
    def __setup__(cls):
        super(Version, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('code_uk1', Unique(t, t.code),
                'incoterm.msg_incoterm_version_code_uk1')]


class Rule(ModelView, ModelSQL):
    """Incoterm rule"""
    __name__ = 'incoterm.rule'
    _rec_name = 'abbreviation'

    abbreviation = fields.Char('Abbreviation', size=3, required=True)
    name = fields.Char('Name', required=True, translate=True)
    versions = fields.Many2Many('incoterm.rule-incoterm.version', 'rule',
        'version', 'Versions')

    @classmethod
    def __setup__(cls):
        super(Rule, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('abb_uk1', Unique(t, t.abbreviation),
                'incoterm.msg_incoterm_rule_abb_uk1')]


class RuleVersion(ModelSQL):
    """Incoterm rule - Version"""
    __name__ = 'incoterm.rule-incoterm.version'
    _table = 'incoterm_rule_version_rel'

    rule = fields.Many2One('incoterm.rule', 'Rule',
        ondelete='CASCADE')
    version = fields.Many2One('incoterm.version', 'Version',
        ondelete='RESTRICT')


class IncotermDocumentMixin(Model):
    """Incoterm Document Mixin"""
    __slots__ = ()

    incoterm_version = fields.Many2One('incoterm.version', 'Incoterm version',
        ondelete='RESTRICT',
        states={'readonly': Eval('state') != 'draft'},
        depends=['state'])

    @classmethod
    def default_incoterm_version(cls):
        Version = Pool().get('incoterm.version')

        versions = Version.search([], order=[('code', 'DESC')])
        if len(versions) == 1:
            return versions[0].id

    def _explode_incoterms(self, incoterms):
        pool = Pool()
        Doc = pool.get(self.__name__)
        Incoterm = pool.get(Doc.incoterms.model_name)

        values = []
        for incoterm in incoterms:
            values.append(Incoterm(
                rule=incoterm.rule,
                place=incoterm.place,
                place_required=incoterm.place_required)
            )
        self.incoterms = values


class IncotermMasterMixin(Model):
    """Incoterm Master Mixin"""
    __slots__ = ()

    rule = fields.Many2One('incoterm.rule', 'Rule',
        ondelete='RESTRICT', required=True)
    place = fields.Char('Place')
    place_required = fields.Boolean('Place required')

    def get_rec_name(self, name):
        if self.place:
            return "%s %s" % (self.rule, self.place)
        return self.rule

    @staticmethod
    def default_place_required():
        return False


class IncotermMixin(object):
    """Incoterm Mixin"""
    __slots__ = ()

    version = fields.Function(
        fields.Many2One('incoterm.version', 'Version'),
        'on_change_with_version')
    rule = fields.Many2One('incoterm.rule', 'Rule',
        ondelete='RESTRICT', required=True,
        domain=[('versions.id', '=', Eval('version', None))],
        depends=['version'])
    value = fields.Numeric(
        'Value', digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits']
    )
    currency = fields.Many2One('currency.currency', 'Currency')
    place = fields.Char('Place', states={
        'required': Bool(Eval('place_required'))
        }, depends=['place_required'])
    currency_digits = fields.Function(
        fields.Integer('Value', depends=['currency']),
        'on_change_with_currency_digits'
    )
    place_required = fields.Boolean('Place required', readonly=True)

    @classmethod
    def __register__(cls, module_name):
        super().__register__(module_name)
        table = cls.__table_handler__(module_name)
        table.not_null_action('place', 'remove')

    def get_rec_name(self, name):
        """Copy at destination class"""
        return '%s %s' % (self.rule.rec_name, self.place)

    def on_change_with_version(self, name=None):
        _field = self._get_relation_version()
        if _field and _field.incoterm_version:
            return _field.incoterm_version.id
        return None

    def _get_relation_version(self):
        pass

    @staticmethod
    def default_currency():
        Company = Pool().get('company.company')

        if 'currency' in Transaction().context:
            return Transaction().context.get('currency')

        company_id = Transaction().context.get('company')
        if company_id:
            return Company(company_id).currency.id
        return None

    @fields.depends('currency')
    def on_change_with_currency_digits(self, name=None):
        if self.currency:
            return self.currency.digits
        return 2

    @staticmethod
    def default_value():
        return Transaction().context.get('value')
